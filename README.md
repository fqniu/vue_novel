### 演示
+ 官网: https://t.shuqi.com
+ 上线网址
    - 后台管理系统：
    - webApp：

- 上线地址：http://www.fqniu.xyz:20020/#/profile

### 项目页面截图
<img src=""./public/jieping/书城.png>
<img src=""./public/jieping/分类.png>
<img src=""./public/jieping/分类下的都市小说类型.png>
<img src=""./public/jieping/都市小说之小说简介.png>
<img src=""./public/jieping/小说目录.png>
<img src=""./public/jieping/注册.png>
<img src=""./public/jieping/登录.bmp>
<img src=""./public/jieping/登录2.bmp>
<img src=""./public/jieping/登录3.bmp>


### 项目目录说明 
```js
alnovel├─public    网站根目录
         │  └─image    
         └─src
            ├─assets        静态资源
            ├─components    组件
            ├─network       网络请求
            ├─router        路由配置
            ├─store         Vuex
            ├─utils         工具包
            └─views         页面
```

